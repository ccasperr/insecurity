package nl.jcore.todo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

@Repository
public class TodoRepository {
    @Value("${spring.datasource.url}")
    private String dbUrl;
    @Value("${spring.datasource.username}")
    private String user;
    @Value("${spring.datasource.password}")
    private String password;

    public String query(String todoName) throws SQLException {
        try (Connection connection = connection()) {
            String query = "SELECT due FROM todos WHERE todo='" + todoName + "'";
            try (Statement statement = connection.createStatement()) {
                try (ResultSet resultSet = statement.executeQuery(query)) {
                    if (resultSet.next()) {

                        return resultSet.getString(1);
                    }
                }
                return "";
            }
        }
    }

    public Connection connection() throws SQLException {
        return DriverManager.getConnection(dbUrl, user, password);
    }
}
