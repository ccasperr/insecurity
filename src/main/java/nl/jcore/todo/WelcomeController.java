package nl.jcore.todo;


import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class WelcomeController {
    private TodoRepository todoRepository;

    // inject via application.properties
    @Value("${welcome.message}")
    private String message;

    private List<String> tasks;

    public WelcomeController(TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }


    @GetMapping("/")
    public String main(Model model) throws SQLException {
        model.addAttribute("message", todoRepository.query("vacuuming"));
        model.addAttribute("tasks", tasks);

        return "welcome"; //view
    }

    @GetMapping("/hello")
    public String mainWithParam(
            @RequestParam(name = "name", required = false, defaultValue = "")
                    String name, Model model) throws SQLException {

        model.addAttribute("message", name);
        model.addAttribute("tasks", List.of(
                todoRepository.query("vacuuming")));
        return "welcome"; //view
    }

}
