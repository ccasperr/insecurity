package nl.jcore.todo;


import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/ping")
public class PingController {

    @PostMapping()
    public String pingme(@RequestParam(name = "host") String input) {
        ProcessBuilder processBuilder = new ProcessBuilder();
        String cmd = "ping -c 1 '" + input + "'";
        System.out.println(cmd);
        processBuilder.command("bash", "-c", cmd);

        StringBuilder output = new StringBuilder();

        try {
            Process process = processBuilder.start();
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));

            String line;
            while ((line = reader.readLine()) != null) {
                output.append(line + "\n");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return output.toString();
    }

}
